// Import React
import React from "react";

// Import Spectacle Core tags
import {
  BlockQuote,
  CodePane,
  Deck,
  Heading,
  Image,
  Slide,
  Spectacle,
  Text
} from "spectacle";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Import custom component
import Terminal from "./terminal";

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");

const images = {
  devduck: require("../assets/devduck.png"),
  brainhub: require("../assets/brainhub.svg")
};

preloader(images);

const codes = {
  functionsVariables: `function triple(x) {
  return x * 3;
}

const multiplyByThree = triple;

console.log(multiplyByThree(2));`,
  higherOrder: `function repeat(n, fn) {
  for(let i = 0; i < n; i++) {
    fn();
  }
}
function print() {
  console.log('repeat me');
}

repeat(3, print);`,
  ajaxCallback: `const request = require('request');

request('http://localhost:3000/api', (err, res, body) => console.log(body));`,
  syncCallback: `function callSync(fn) {
  return fn();
}

callSync(() => console.log('callback function'));
console.log('after');`,
  asyncCallback: `function callAsync(fn) {
  setTimeout(fn, 0);
}

callAsync(() => console.log('callback function'));
console.log('after');`,
  goodPracticeCallback: `function validate(email, cb) {
  if (email.match(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+(\\\.[a-zA-Z0-9]+)+$/)) {
    return cb(null, email);
  }
  return cb(new Error('incorrect email address'));
}

validate('email@example.com', (err, email) => {
  if (err) {
    return console.log(err);
  }
  console.log('Successfully validated:', email);
});`,
  ioCallback: `const fs = require('fs');

fs.readFile('./file.txt', 'utf-8', (err, data) => {
  if(!err) {
    console.log(data);
  }
});`,
  callbackHell: `const request = require('request');
const fs = require('fs');

request('http://localhost:3000/api', (err, res, body) => {
  if (!err) {
    fs.readFile('./file.txt', 'utf-8', (err, data) => {
      if (!err) {
        [data, body].map((text, index) => {
          console.log(index, text);
        });
      }
    });
  }
});`,
  noMoreCallbackHell: `const fetch = require('node-fetch');
const fs = require('fs');

function readFilePromised(file) {
  const p = new Promise((resolve, reject) => {
    fs.readFile(file, 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
  return p;
}

Promise.all([
  readFilePromised('./file.txt'),
  fetch('http://localhost:3000/api').then(r => r.text())
])
.then((responses) => responses.map((text, index) => {
  console.log(index, text);
}));`,
  promise: `const a = 4;
const b = 2;
const p = new Promise((resolve, reject) => {
  if (b === 0) {
    reject(new Error('Do not divide by zero'));
  }
  resolve(a / b);
});

p.then(value => console.log(value)).catch(err => console.error(err));`,
  fetch: `const fetch = require('node-fetch'); // there is no window.fetch on node.js

fetch('http://localhost:3000/api')
.then(res => res.text())
.then(body => console.log(body));`,
  numbersGenerator: `function *naturalNumbers() {
  let x = 0;
  while (true) {
    x = x + 1;
    yield x;
  }
}

const generator = naturalNumbers();
console.log(generator.next());
console.log(generator.next());
console.log(generator.next());`,
  asyncGenerator: `const fetch = require('node-fetch');
const co = require('co');

co(function *() {
  const uri = 'http://localhost:3000/api';
  const response = yield fetch(uri);
  const body = yield response.text();
  console.log(body);
});`,
  coImplementation: `const fetch = require('node-fetch');

function co(generator) {
  const iterator = generator();
  const iteration = iterator.next();
  function iterate(iteration) {
    if (iteration.done) {
      return iteration.value;
    }
    const promise = iteration.value;
    return promise.then(x => iterate(iterator.next(x)));
  }
  return iterate(iteration);
}

co(function *() {
  const uri = 'http://localhost:3000/api';
  const response = yield fetch(uri);
  const body = yield response.text();
  console.log(body);
});`
};

const theme = createTheme({
  primary: "#ffa645"
});

export default class Presentation extends React.Component {
  render() {
    return (
      <Spectacle theme={theme}>
        <Deck transition={["slide"]} transitionDuration={500} progress="pacman" controls={false}>
          <Slide bgColor="primary">
            <Heading size={1} caps textColor="black" textAlign="left" margin="3% auto">
              Callbacks,
            </Heading>
            <Heading size={1} caps textColor="black" textAlign="center" margin="3% auto">
              Promises,
            </Heading>
            <Heading size={1} caps textColor="black" textAlign="right" margin="3% auto">
              Generators
            </Heading>
            <Image width="100%" margin="5% auto 0" src={images.devduck}/>
          </Slide>
          <Slide>
            <Heading size={1}>Adam Gołąb</Heading>
            <Heading size={4}>Full Stack Developer at</Heading>
            <Image src={images.brainhub} width="40%"/>
          </Slide>
          {/* <Slide notes="W JS funkcje traktowane są podobnie jak wszelkie inne zmienne. Możemy przypisać je do zmiennych. Tworzenie funkcji anonimowych. A także przekazywać jako parametr do innych funkcji">
            <Heading size={1} fit>Functions == Variables</Heading>
            <Terminal defaultValue={codes.functionsVariables}/>
          </Slide>
          <Slide notes="Funkcje wyższego rzędu. Funkcje które przyjmują co najmniej jedną funkcję jako argument">
            <Heading size={1} fit margin="5% auto">Higher-order functions</Heading>
            <Text>
              In mathematics and computer science, a higher-order function (also functional, functional form or functor)
              is a function that takes one or more functions as arguments.
            </Text>
          </Slide>
          <Slide>
            <Heading size={1} fit>Higher-order functions</Heading>
            <Terminal defaultValue={codes.higherOrder}/>
          </Slide> */}
          <Slide>
            <Heading size={1} fit margin="5% auto">Callbacks</Heading>
            <Text>
              In computer programming, a callback is a piece of executable code that is passed as an argument to other code,
              which is expected to call back (execute) the argument at some convenient time.
            </Text>
          </Slide>
          {/* <Slide>
            <Heading size={1}>Sync Callback</Heading>
            <Terminal defaultValue={codes.syncCallback} />
          </Slide> */}
          <Slide notes="W JS dobrą praktyką jest pisanie callbacków które przyjmują dwa parametry, error i wynik wywołania funkcji.">
            <Heading size={1}>Good Practice</Heading>
            <Terminal defaultValue={codes.goodPracticeCallback} />
          </Slide>
          <Slide>
            <Heading size={1} fit margin="5% auto">Asynchronous</Heading>
            <Text>For a function to be asynchronous it needs to perform an asynchronous operation. It needs to incorporate
            the argument callback in handling the results of this asynchronous operation. Only this way the function becomes
            asynchronous.</Text>
          </Slide>
          <Slide>
            <Heading size={1}>Async Callback</Heading>
            <Terminal defaultValue={codes.asyncCallback} />
          </Slide>
          <Slide>
            <Heading size={1}>AJAX request</Heading>
            <Terminal defaultValue={codes.ajaxCallback}/>
          </Slide>
          <Slide>
            <Heading size={1}>IO operation</Heading>
            <Terminal defaultValue={codes.ioCallback}/>
          </Slide>
          <Slide>
            <Heading size={1}>Pyramid from hell</Heading>
            <Terminal defaultValue={codes.callbackHell}/>
          </Slide>
          <Slide>
            <Heading size={1}>Promises</Heading>
            <Text>
              The Promise object is used for asynchronous computations.
              A Promise represents a value which may be available now, or in the future, or never.
            </Text>
            <CodePane
              lang="js"
              source="new Promise( function(resolve, reject) { ... } );"
              margin="40px auto"
              textSize="2rem"
            />
          </Slide>
          <Slide notes="Zwraca tylko jeden parametr, resolve po reject nie działa">
            <Heading>Promises</Heading>
            <Terminal defaultValue={codes.promise}/>
          </Slide>
          <Slide>
            <Heading size={1}>
              <span style={{ color: "#000000", textDecoration: "line-through" }}>
                <span style={{ color: "#ffffff" }}>
                  AJAX
                </span>
              </span> Fetch</Heading>
            <Terminal defaultValue={codes.fetch}/>
          </Slide>
          <Slide>
            <Heading size={1} fit>No more callback hell</Heading>
            <Terminal defaultValue={codes.noMoreCallbackHell}/>
          </Slide>
          <Slide>
            <Heading size={1}>Generators</Heading>
            <BlockQuote textSize="4rem">Pausable functions</BlockQuote>
            <Text>is a special routine that can be used to control the iteration behaviour of a loop.
            In fact, all generators are iterators</Text>
          </Slide>
          <Slide notes="Działanie return, przyjmowanie parametru z yield">
            <Heading size={1} fit>Natural numbers generator</Heading>
            <Terminal defaultValue={codes.numbersGenerator} />
          </Slide>
          <Slide>
            <Heading size={1}>Async generator</Heading>
            <Terminal defaultValue={codes.asyncGenerator} />
          </Slide>
          <Slide>
            <Heading size={1} fit>Co function implementation</Heading>
            <Terminal defaultValue={codes.coImplementation}/>
          </Slide>
          <Slide>
            <Heading size={1} fit>Questions?</Heading>
          </Slide>
          {/* <Slide>
            <Heading>We are hiring!</Heading>
            <Image src={images.brainhub} width="70%"/>
            <Text>http://brainhub.eu/carrer</Text>
          </Slide> */}
        </Deck>
      </Spectacle>
    );
  }
}
