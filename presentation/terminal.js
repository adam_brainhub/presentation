import React, { Component, PropTypes } from "react";
import AceEditor from "react-ace";
import io from "socket.io-client";

import "brace/mode/javascript";
import "brace/mode/plain_text";
import "brace/theme/chrome";
import "brace/ext/language_tools";

require("./terminal.css");

export default class Terminal extends Component {
  static propTypes = {
    defaultValue: PropTypes.string
  }

  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSocket = this.handleSocket.bind(this);
  }

  state = {}

  componentDidMount() {
    this.socket = io();
    this.socket.on("response", this.handleSocket);
    this.socket.on("error", this.handleSocket);
  }

  handleSocket(data) {
    this.setState({
      ...this.state,
      result: this.state.result + data
    });
  }

  handleClick() {
    this.setState({
      ...this.state,
      result: ""
    });
    this.socket.emit("command", typeof (this.state.editor) !== "undefined" ? this.state.editor : this.props.defaultValue);
  }

  handleChange(value) {
    this.setState({
      ...this.state,
      editor: value
    });
  }

  render() {
    return (
      <div>
        <AceEditor
          mode="javascript"
          theme="chrome"
          name="AceEditor"
          className="terminal"
          width="100%"
          minLines={10}
          maxLines={10}
          tabSize={2}
          editorProps={{ $blockScrolling: true }}
          setOptions={{
            enableBasicAutocompletion: true,
            enableLiveAutocompletion: true,
            tabSize: 2,
            fontSize: 18
          }}
          value={ typeof (this.state.editor) !== "undefined" ? this.state.editor : this.props.defaultValue }
          onChange={this.handleChange}
        />
        <button onClick={this.handleClick} className="button">Run &#9658;</button>
        <AceEditor
          mode="plain_text"
          theme="chrome"
          name="AceTerminal"
          className="terminal"
          width="100%"
          minLines={3}
          maxLines={5}
          tabSize={2}
          editorProps={{ $blockScrolling: true }}
          setOptions={{
            tabSize: 2,
            fontSize: 18
          }}
          value={this.state.result}
          readOnly
          showGutter={false}
        />
      </div>
    );
  }
}

Terminal.propTypes = {
  value: PropTypes.string
};
