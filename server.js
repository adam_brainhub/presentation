/* eslint-disable */

var path = require("path");
var express = require("express");
var webpack = require("webpack");
var config = require("./webpack.config");

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var pty = require('pty.js');
var spawn = require('child_process').spawn;

var compiler = webpack(config);

var serverPort = process.env.PORT || 3000;

app.use(require("webpack-dev-middleware")(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require("webpack-hot-middleware")(compiler));

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.get("/api", function(req, res) {
  res.status(200).end('Response from server');
});

io.on('connection', function(socket) {
  socket.on('command', function(msg) {
    var term = spawn('node');
    term.stdout.setEncoding('utf-8');
    term.stderr.setEncoding('utf-8');
    term.stdin.setEncoding('utf-8');

    term.stdout.on('data', function(data) {
      io.emit('response', data);
    });

    term.stderr.on('data', function(err) {
      io.emit('error', err);
    });

    term.stdin.write(msg + '\n');
    term.stdin.end();
  });
});

http.listen(serverPort, "localhost", function (err) {
  if (err) {
    console.log(err);
    return;
  }

  console.log("Listening at http://localhost:" + serverPort);
});
